/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  // url de l'API node
  apiUrlService: 'https://maderaapi.kvly.fr',
  // url de base de l'application
  baseUrl: 'https://madera.kvly.fr:4200',
  // domaine
  baseDomain: 'localhost',
};
