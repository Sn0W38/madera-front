# Projet Madera Frontend
Le projet MADERA est un projet de groupe fictif fil rouge dans le cadre de mon alternance au CESI.
Il a pour but de:
* Gérer un projet information de A a Z
* Concevoir une maquette
* Créer une application à but "fictif" de gestion de maison modulaire

[Inituler du projet](https://drive.google.com/file/d/1-gsgc8VufGW-BjRhYYxeNYMr3e42mtpx/view?usp=sharing)
  
## Membre du groupe:
* [Kevin Landry](https://kvly.fr)
* [Teddy Sommavilla](https://github.com/Wazazaby)

## Projet :
Le projet ce décompose en 2 parties:
* [Un frontend](https://gitlab.com/Sn0W38/madera-front)
* [Une API](https://gitlab.com/Sn0W38/madera-api)

### Compte de demo
* User: romeo.elvis@madera.com
* MDP: motdepasse

### Librairie utiliser :
* bootstrap (css-only)
* font-awesome V6
* charts.js
